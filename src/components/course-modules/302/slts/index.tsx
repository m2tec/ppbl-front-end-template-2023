import Link from "next/link";
import React from "react";

import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Box, Button, Divider, Flex, Spacer } from "@chakra-ui/react";

import Introduction from "./Introduction.mdx";

const SLTs = () => {
  return (
    <Box w="95%" marginTop="2em">
      <SLTsItems moduleTitle="Module 302" moduleNumber={302} />
      <Divider mt="5" />
      <Box py="5">
        <Introduction />
      </Box>
      <Flex direction="row">
        <Spacer />
      <Link href="/modules/302/3021">
        <Button colorScheme="green" my="1em">Get Started</Button>
      </Link>
      <Spacer />
      </Flex>
    </Box>
  );
};

export default SLTs;